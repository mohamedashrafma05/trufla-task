import {
  HttpServer,
  HttpStatus,
  INestApplication,
  ValidationPipe,
} from '@nestjs/common';
import { TestingModule, Test } from '@nestjs/testing';
import { LikesModule } from '../../src/likes/likes.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as request from 'supertest';
import { AddLikeDto } from 'src/likes/dto/add-like.dto';

describe('[Feature] Likes - /likes', () => {
  const like = {
    author: {
      id: 1,
    },
    article: {
      id: 1,
    },
  };
  const expectedPartialLike = jasmine.objectContaining({
    ...like,
  });
  let app: INestApplication;
  let httpServer: HttpServer;
  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        LikesModule,
        TypeOrmModule.forRoot({
          type: 'mysql',
          host: 'localhost',
          port: 3307,
          username: 'root',
          password: 'admin',
          database: 'test',
          autoLoadEntities: true,
          synchronize: true,
        }),
      ],
    }).compile();
    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(
      new ValidationPipe({
        whitelist: true,
        transform: true,
        forbidNonWhitelisted: true,
        transformOptions: { enableImplicitConversion: true },
      }),
    );
    await app.init();
    httpServer = app.getHttpServer();
  });

  it('Create [POST /]', () => {
    return request(httpServer)
      .post('/likes')
      .send(like as AddLikeDto)
      .expect(HttpStatus.CREATED)
      .then(({ body }) => {
        expect(body).toEqual(expectedPartialLike);
      });
  });

  afterAll(async () => {
    await app.close();
  });
});
