import {
  HttpServer,
  HttpStatus,
  INestApplication,
  ValidationPipe,
} from '@nestjs/common';
import { TestingModule, Test } from '@nestjs/testing';
import { AuthorsModule } from '../../src/authors/authors.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as request from 'supertest';
import { CreateAuthorDto } from 'src/authors/dto/create-author.dto';

describe('[Feature] Authors - /authors', () => {
  const author = {
    name: 'author',
    jobTitle: 'author',
  };
  const expectedPartialAuthor = jasmine.objectContaining({
    ...author,
  });
  let app: INestApplication;
  let httpServer: HttpServer;
  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        AuthorsModule,
        TypeOrmModule.forRoot({
          type: 'mysql',
          host: 'localhost',
          port: 3307,
          username: 'root',
          password: 'admin',
          database: 'test',
          autoLoadEntities: true,
          synchronize: true,
        }),
      ],
    }).compile();
    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(
      new ValidationPipe({
        whitelist: true,
        transform: true,
        forbidNonWhitelisted: true,
        transformOptions: { enableImplicitConversion: true },
      }),
    );
    await app.init();
    httpServer = app.getHttpServer();
  });

  it('Create [POST /]', () => {
    return request(httpServer)
      .post('/authors')
      .send(author as CreateAuthorDto)
      .expect(HttpStatus.CREATED)
      .then(({ body }) => {
        expect(body).toEqual(expectedPartialAuthor);
      });
  });
  it('Get all [GET /]', () => {
    return request(httpServer)
      .get('/authors')
      .then(({ body }) => {
        console.log(body);
        expect(body.length).toBeGreaterThan(0);
        expect(body[0]).toEqual(expectedPartialAuthor);
      });
  });
  it('Get one [GET /:id]', () => {
    return request(httpServer)
      .get('/authors/1')
      .then(({ body }) => {
        expect(body).toEqual(expectedPartialAuthor);
      });
  });

  afterAll(async () => {
    await app.close();
  });
});
