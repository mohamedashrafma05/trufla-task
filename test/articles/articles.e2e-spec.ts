import {
  HttpServer,
  HttpStatus,
  INestApplication,
  ValidationPipe,
} from '@nestjs/common';
import { TestingModule, Test } from '@nestjs/testing';
import { ArticlesModule } from '../../src/articles/articles.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as request from 'supertest';
import { CreateArticleDto } from 'src/articles/dto/create-article.dto';

describe('[Feature] Articles - /articles', () => {
  const article = {
    title: 'new article',
    body: 'new body',
    author: {
      id: 1,
    },
  };
  const expectedPartialArticle = jasmine.objectContaining({
    ...article,
  });
  let app: INestApplication;
  let httpServer: HttpServer;
  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        ArticlesModule,
        TypeOrmModule.forRoot({
          type: 'mysql',
          host: 'localhost',
          port: 3307,
          username: 'root',
          password: 'admin',
          database: 'test',
          autoLoadEntities: true,
          synchronize: true,
        }),
      ],
    }).compile();
    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(
      new ValidationPipe({
        whitelist: true,
        transform: true,
        forbidNonWhitelisted: true,
        transformOptions: { enableImplicitConversion: true },
      }),
    );
    await app.init();
    httpServer = app.getHttpServer();
  });

  it('Create [POST /]', () => {
    return request(httpServer)
      .post('/articles')
      .send(article as CreateArticleDto)
      .expect(HttpStatus.CREATED)
      .then(({ body }) => {
        expect(body).toEqual(expectedPartialArticle);
      });
  });
  it('Get all [GET /]', () => {
    return request(httpServer)
      .get('/articles')
      .then(({ body }) => {
        console.log(body);
        expect(body.length).toBeGreaterThan(0);
        expect(body[0]).toEqual(expectedPartialArticle);
      });
  });
  it('Get one [GET /:id]', () => {
    return request(httpServer)
      .get('/articles/1')
      .then(({ body }) => {
        expect(body).toEqual(expectedPartialArticle);
      });
  });

  afterAll(async () => {
    await app.close();
  });
});
