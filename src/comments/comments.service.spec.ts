import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { CommentsService } from './comments.service';
import { Like } from './entities/like.entity';
import { Comment } from './entities/comment.entity';
import { Article } from './entities/article.entity';
import { Author } from './entities/author.entity';

type MockRepository<T = any> = Partial<Record<keyof Repository<T>, jest.Mock>>;
const createMockRepository = <T = any>(): MockRepository<T> => ({
  create: jest.fn(),
  save: jest.fn(),
});

describe('CommentsService', () => {
  let service: CommentsService;
  let commentRepository: MockRepository;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CommentsService,
        { provide: Connection, useValue: {} },
        {
          provide: getRepositoryToken(Article),
          useValue: createMockRepository(),
        },
        {
          provide: getRepositoryToken(Author),
          useValue: createMockRepository(),
        },
        { provide: getRepositoryToken(Like), useValue: createMockRepository() },
        {
          provide: getRepositoryToken(Comment),
          useValue: createMockRepository(),
        },
      ],
    }).compile();

    service = module.get<CommentsService>(CommentsService);
    commentRepository = module.get<MockRepository>(getRepositoryToken(Comment));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create', () => {
    describe('when creating a comment', () => {
      it('should return the comment object', async () => {
        const newComment = {
          body: 'new body',
          author: {
            id: 1,
          },
          article: {
            id: 1,
          },
        };
        const expectedComment = {};
        const expectedSavedComment = {};
        const commentRepositoryCreate = commentRepository.create.mockReturnValue(
          expectedComment,
        );
        const commentRepositorySave = commentRepository.save.mockResolvedValue(
          expectedSavedComment,
        );
        const comment = await service.create(newComment);
        expect(commentRepositoryCreate).toBeCalledWith(newComment);
        expect(commentRepositorySave).toBeCalledWith(expectedSavedComment);
        expect(comment).toEqual(expectedComment);
      });
    });
  });
});
