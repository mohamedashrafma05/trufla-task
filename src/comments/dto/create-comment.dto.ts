import { IsString, ValidateNested, IsNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
class IdDto {
  @IsNumber()
  readonly id: number;
}
export class CreateCommentDto {
  @ApiProperty({ description: 'The body of the comment.' })
  @IsString()
  readonly body: string;

  @ApiProperty({ description: 'The id of the author that make the comment.' })
  @ValidateNested()
  @Type(() => IdDto)
  readonly author: IdDto;

  @ApiProperty({ description: 'The id of the commented article .' })
  @ValidateNested()
  @Type(() => IdDto)
  readonly article: IdDto;
}
