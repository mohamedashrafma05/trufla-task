import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ArticlesModule } from './articles/articles.module';
import { AuthorsModule } from './authors/authors.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { Article } from './articles/entities/article.entity';
import { Author } from './authors/entities/author.entity';
import { Comment } from './comments/entities/comment.entity';
import { Like } from './likes/entities/like.entity';
import { CommentsModule } from './comments/comments.module';
import { LikesModule } from './likes/likes.module';
@Module({
  imports: [
    CommentsModule,
    ArticlesModule,
    AuthorsModule,
    TypeOrmModule.forRoot(),
    LikesModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private connection: Connection) {}
}
