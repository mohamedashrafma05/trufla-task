import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { AuthorsService } from './authors.service';
import { Like } from './entities/like.entity';
import { Comment } from './entities/comment.entity';
import { Article } from './entities/article.entity';
import { Author } from './entities/author.entity';
import { NotFoundException } from '@nestjs/common';

type MockRepository<T = any> = Partial<Record<keyof Repository<T>, jest.Mock>>;
const createMockRepository = <T = any>(): MockRepository<T> => ({
  findOne: jest.fn(),
  find: jest.fn(),
  create: jest.fn(),
  save: jest.fn(),
});
describe('AuthorsService', () => {
  let service: AuthorsService;
  let authorRepository: MockRepository;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthorsService,
        { provide: Connection, useValue: {} },
        {
          provide: getRepositoryToken(Article),
          useValue: createMockRepository(),
        },
        {
          provide: getRepositoryToken(Author),
          useValue: createMockRepository(),
        },
        { provide: getRepositoryToken(Like), useValue: createMockRepository() },
        {
          provide: getRepositoryToken(Comment),
          useValue: createMockRepository(),
        },
      ],
    }).compile();

    service = module.get<AuthorsService>(AuthorsService);
    authorRepository = module.get<MockRepository>(getRepositoryToken(Author));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findOne', () => {
    describe('when author with ID exists', () => {
      it('should return the author object', async () => {
        const authorId = 1;
        const expectedAuthor = {};

        authorRepository.findOne.mockReturnValue(expectedAuthor);
        const author = await service.findOne(authorId);
        expect(author).toEqual(expectedAuthor);
      });
    });
    describe('otherwise', () => {
      it('should throw the "NotFoundException"', async () => {
        const authorId = 1;
        authorRepository.findOne.mockReturnValue(undefined);

        try {
          await service.findOne(authorId);
        } catch (err) {
          expect(err).toBeInstanceOf(NotFoundException);
          expect(err.message).toEqual(`author #${authorId} not found`);
        }
      });
    });
  });
  describe('findAll', () => {
    describe('when author exists', () => {
      it('should return the authors objects', async () => {
        const expectedAuthor = {};

        authorRepository.find.mockReturnValue(expectedAuthor);
        const author = await service.findAll();
        expect(author).toEqual(expectedAuthor);
      });
    });
  });

  describe('create', () => {
    describe('when creating an author', () => {
      it('should return the author object', async () => {
        const newAuthor = {
          name: 'new author',
          jobTitle: 'author',
        };
        const expectedAuthor = {};
        const expectedSavedAuthor = {};
        const authorRepositoryCreate = authorRepository.create.mockReturnValue(
          expectedAuthor,
        );
        const authorRepositorySave = authorRepository.save.mockResolvedValue(
          expectedSavedAuthor,
        );
        const author = await service.create(newAuthor);
        expect(authorRepositoryCreate).toBeCalledWith(newAuthor);
        expect(authorRepositorySave).toBeCalledWith(expectedSavedAuthor);
        expect(author).toEqual(expectedAuthor);
      });
    });
  });
});
