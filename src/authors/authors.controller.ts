import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  Query,
  Res,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AuthorsService } from './authors.service';
import { CreateAuthorDto } from './dto/create-author.dto';

@ApiTags('authors')
@Controller('authors')
export class AuthorsController {
  constructor(private readonly authorService: AuthorsService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  addAuthor(@Body() body: CreateAuthorDto) {
    return this.authorService.create(body);
  }

  @Get()
  getAuthors() {
    return this.authorService.findAll();
  }

  @Get('/:id')
  getOneAuthor(@Param('id') id: number) {
    return this.authorService.findOne(id);
  }
}
