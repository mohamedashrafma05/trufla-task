import { IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
export class CreateAuthorDto {
  @ApiProperty({ description: 'The name of the author.' })
  @IsString()
  readonly name: string;

  @ApiProperty({ description: 'The job tsitle of the author.' })
  @IsString()
  readonly jobTitle: string;
}
