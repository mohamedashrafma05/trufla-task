import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Article } from './article.entity';
import { Author } from './author.entity';
@Entity()
export class Comment {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  body: string;

  @ManyToOne(
    type => Author,
    author => author.comments,
  )
  author: Author;

  @ManyToOne(
    type => Article,
    article => article.comments,
  )
  article: Article;
}
