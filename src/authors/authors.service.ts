import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Author } from './entities/author.entity';
import { CreateAuthorDto } from './dto/create-author.dto';
@Injectable()
export class AuthorsService {
  constructor(
    @InjectRepository(Author)
    private authorsRepository: Repository<Author>,
  ) {}
  create(body: CreateAuthorDto) {
    const author = this.authorsRepository.create(body);
    const result = this.authorsRepository.save(author);
    return result;
  }
  findAll(): Promise<Author[]> {
    return this.authorsRepository.find({
      relations: ['articles', 'comments', 'likes'],
    });
  }

  async findOne(id: number): Promise<Author> {
    const author = await this.authorsRepository.findOne(id, {
      relations: ['articles', 'comments', 'likes'],
    });
    if (!author) throw new NotFoundException(`author #${id} not found`);
    return author;
  }
}
