import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  Query,
  Res,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ArticlesService } from './articles.service';
import { CreateArticleDto } from './dto/create-article.dto';
import { PaginationQueryDto } from './dto/pagination-query.dto';

@ApiTags('articles')
@Controller('articles')
export class ArticlesController {
  constructor(private readonly articleService: ArticlesService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  addbody(@Body() body: CreateArticleDto) {
    return this.articleService.create(body);
  }

  @Get()
  getCoffes() {
    return this.articleService.findAll();
  }

  @Get('/search')
  serach(@Query() paginationQuery: PaginationQueryDto) {
    return this.articleService.search(paginationQuery);
  }

  @Get('/:id')
  getOneCoffee(@Param('id') id: number) {
    return this.articleService.findOne(id);
  }
}
