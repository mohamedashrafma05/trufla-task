import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { ArticlesService } from './articles.service';
import { Article } from './entities/article.entity';
import { Author } from './entities/author.entity';
import { Like } from './entities/like.entity';
import { Comment } from './entities/comment.entity';
import { NotFoundException } from '@nestjs/common';
import { PaginationQueryDto } from './dto/pagination-query.dto';

type MockRepository<T = any> = Partial<Record<keyof Repository<T>, jest.Mock>>;
const createMockRepository = <T = any>(): MockRepository<T> => ({
  findOne: jest.fn(),
  find: jest.fn(),
  create: jest.fn(),
  save: jest.fn(),
});

describe('ArticlesService', () => {
  let service: ArticlesService;
  let articleRepository: MockRepository;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ArticlesService,
        { provide: Connection, useValue: {} },
        {
          provide: getRepositoryToken(Article),
          useValue: createMockRepository(),
        },
        {
          provide: getRepositoryToken(Author),
          useValue: createMockRepository(),
        },
        { provide: getRepositoryToken(Like), useValue: createMockRepository() },
        {
          provide: getRepositoryToken(Comment),
          useValue: createMockRepository(),
        },
      ],
    }).compile();

    service = module.get<ArticlesService>(ArticlesService);
    articleRepository = module.get<MockRepository>(getRepositoryToken(Article));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findOne', () => {
    describe('when article with ID exists', () => {
      it('should return the article object', async () => {
        const articleId = 1;
        const expectedArticle = {};

        articleRepository.findOne.mockReturnValue(expectedArticle);
        const article = await service.findOne(articleId);
        expect(article).toEqual(expectedArticle);
      });
    });
    describe('otherwise', () => {
      it('should throw the "NotFoundException"', async () => {
        const articleId = 1;
        articleRepository.findOne.mockReturnValue(undefined);

        try {
          await service.findOne(articleId);
        } catch (err) {
          expect(err).toBeInstanceOf(NotFoundException);
          expect(err.message).toEqual(`article #${articleId} not found`);
        }
      });
    });
  });
  describe('findAll', () => {
    describe('when article exists', () => {
      it('should return the articles objects', async () => {
        const expectedArticle = {};

        articleRepository.find.mockReturnValue(expectedArticle);
        const article = await service.findAll();
        expect(article).toEqual(expectedArticle);
      });
    });
  });
  // describe('search', () => {
  //   describe('when searching for an article', () => {
  //     it('should return the articles objects', async () => {
  //       const expectedArticle = {};
  //       const { prototype } = PaginationQueryDto;
  //       const createQueryBuilder: any = {
  //         leftJoinAndSelect: () => createQueryBuilder,
  //         where: () => createQueryBuilder,
  //         getMany: () => createQueryBuilder,
  //       };
  //       prototype.search = 'new article';
  //       articleRepository.createQueryBuilder.mockImplementation(
  //         () => createQueryBuilder,
  //       );
  //       const article = await service.search(prototype);
  //       expect(article).toEqual(expectedArticle);
  //     });
  //   });
  // });
  describe('create', () => {
    describe('when creating an article', () => {
      it('should return the article object', async () => {
        const newArticle = {
          title: 'new article',
          body: 'new body',
          author: {
            id: 1,
          },
        };
        const expectedArticle = {};
        const expectedSavedArticle = {};
        const articleRepositoryCreate = articleRepository.create.mockReturnValue(
          expectedArticle,
        );
        const articleRepositorySave = articleRepository.save.mockResolvedValue(
          expectedSavedArticle,
        );
        const article = await service.create(newArticle);
        expect(articleRepositoryCreate).toBeCalledWith(newArticle);
        expect(articleRepositorySave).toBeCalledWith(expectedSavedArticle);
        expect(article).toEqual(expectedArticle);
      });
    });
  });
});
