import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Article } from './entities/article.entity';
import { CreateArticleDto } from './dto/create-article.dto';
import { PaginationQueryDto } from './dto/pagination-query.dto';
@Injectable()
export class ArticlesService {
  constructor(
    @InjectRepository(Article)
    private articlesRepository: Repository<Article>,
  ) {}
  create(body: CreateArticleDto) {
    const article = this.articlesRepository.create(body);
    const result = this.articlesRepository.save(article);
    return result;
  }
  findAll(): Promise<Article[]> {
    return this.articlesRepository.find({
      relations: ['author', 'comments', 'likes'],
    });
  }

  async search(paginationQuery: PaginationQueryDto): Promise<Article[]> {
    const { search } = paginationQuery;
    const article = await this.articlesRepository
      .createQueryBuilder('article')
      .leftJoinAndSelect('article.author', 'author')
      .leftJoinAndSelect('article.likes', 'likes')
      .leftJoinAndSelect('article.comments', 'comments')
      .where('article.title = :title OR article.body = :body', {
        title: search,
        body: search,
      })
      .getMany();
    return article;
  }

  async findOne(id: number): Promise<Article> {
    const article = await this.articlesRepository.findOne(id, {
      relations: ['author', 'comments', 'likes'],
    });
    if (!article) throw new NotFoundException(`article #${id} not found`);
    return article;
  }
}
