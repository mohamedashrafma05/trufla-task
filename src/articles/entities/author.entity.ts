import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Article } from './article.entity';
import { Comment } from './comment.entity';
import { Like } from './like.entity';
@Entity()
export class Author {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  jobTitle: string;

  @OneToMany(
    type => Article,
    article => article.author,
  )
  articles: Article[];

  @OneToMany(
    type => Comment,
    comment => comment.author,
  )
  comments: Comment[];

  @OneToMany(
    type => Like,
    like => like.author,
  )
  likes: Like[];
}
