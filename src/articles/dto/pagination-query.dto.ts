import { IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
export class PaginationQueryDto {
  @ApiProperty({ description: 'The search word for an article.' })
  @IsString()
  search: string;
}
