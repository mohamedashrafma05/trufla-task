import { IsString, IsNumber, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
class AuthorDto {
  @IsNumber()
  readonly id: number;
}
export class CreateArticleDto {
  @ApiProperty({ description: 'The title of an article.' })
  @IsString()
  readonly title: string;

  @ApiProperty({ description: 'The body of an article.' })
  @IsString()
  readonly body: string;
  @ApiProperty({ description: 'The id of the author.' })
  @ValidateNested()
  @Type(() => AuthorDto)
  readonly author: AuthorDto;
}
