import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Article } from './article.entity';
import { Author } from './author.entity';
@Entity()
export class Like {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(
    type => Author,
    author => author.likes,
  )
  author: Author;

  @ManyToOne(
    type => Article,
    article => article.likes,
  )
  article: Article;
}
