import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { Author } from './author.entity';
import { Comment } from './comment.entity';
import { Like } from './like.entity';

@Entity()
export class Article {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  body: string;

  @ManyToOne(
    type => Author,
    author => author.articles,
  )
  author: Author;

  @OneToMany(
    type => Comment,
    comment => comment.article,
  )
  comments: Comment[];

  @OneToMany(
    type => Like,
    like => like.article,
  )
  likes: Like[];
}
