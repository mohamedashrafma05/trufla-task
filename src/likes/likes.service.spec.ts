import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { LikesService } from './likes.service';
import { Article } from './entities/article.entity';
import { Author } from './entities/author.entity';
import { Like } from './entities/like.entity';
import { Comment } from './entities/comment.entity';

type MockRepository<T = any> = Partial<Record<keyof Repository<T>, jest.Mock>>;
const createMockRepository = <T = any>(): MockRepository<T> => ({
  create: jest.fn(),
  save: jest.fn(),
});

describe('LikesService', () => {
  let service: LikesService;
  let LikeRepository: MockRepository;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        LikesService,
        { provide: Connection, useValue: {} },
        {
          provide: getRepositoryToken(Article),
          useValue: createMockRepository(),
        },
        {
          provide: getRepositoryToken(Author),
          useValue: createMockRepository(),
        },
        { provide: getRepositoryToken(Like), useValue: createMockRepository() },
        {
          provide: getRepositoryToken(Comment),
          useValue: createMockRepository(),
        },
      ],
    }).compile();

    service = module.get<LikesService>(LikesService);
    LikeRepository = module.get<MockRepository>(getRepositoryToken(Like));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create', () => {
    describe('when adding a like', () => {
      it('should return the like object', async () => {
        const newLike = {
          author: {
            id: 1,
          },
          article: {
            id: 1,
          },
        };
        const expectedLike = {};
        const expectedSavedLike = {};
        const likeRepositoryCreate = LikeRepository.create.mockReturnValue(
          expectedLike,
        );
        const likeRepositorySave = LikeRepository.save.mockResolvedValue(
          expectedSavedLike,
        );
        const like = await service.create(newLike);
        expect(likeRepositoryCreate).toBeCalledWith(newLike);
        expect(likeRepositorySave).toBeCalledWith(expectedSavedLike);
        expect(like).toEqual(expectedLike);
      });
    });
  });
});
