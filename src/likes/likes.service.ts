import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Like } from './entities/like.entity';
import { AddLikeDto } from './dto/add-like.dto';
@Injectable()
export class LikesService {
  constructor(
    @InjectRepository(Like)
    private likesRepository: Repository<Like>,
  ) {}
  create(body: AddLikeDto) {
    const author = this.likesRepository.create(body);
    const result = this.likesRepository.save(author);
    return result;
  }
}
