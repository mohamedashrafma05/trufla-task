import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { LikesService } from './likes.service';
import { AddLikeDto } from './dto/add-like.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('likes')
@Controller('likes')
export class LikesController {
  constructor(private readonly LikeService: LikesService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  addbody(@Body() body: AddLikeDto) {
    return this.LikeService.create(body);
  }
}
