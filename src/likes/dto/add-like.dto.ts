import { ValidateNested, IsNumber } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
class IdDto {
  @IsNumber()
  readonly id: number;
}
export class AddLikeDto {
  @ApiProperty({ description: 'The id of the author that liked the article.' })
  @ValidateNested()
  @Type(() => IdDto)
  readonly author: IdDto;

  @ApiProperty({ description: 'The id of the liked article.' })
  @ValidateNested()
  @Type(() => IdDto)
  readonly article: IdDto;
}
